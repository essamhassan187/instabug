#!/bin/bash
set -e

# sqlhost="mysql"

# until mysql -h "$sqlhost" -U "root"; do
#   >&2 echo "Mysql is unavailable - sleeping"
#   sleep 1
# done

# >&2 echo "Mysql is up - executing command"

bundle exec rails db:create;
bundle exec rails db:migrate;
bundle exec rails server -b 0.0.0.0;