# !/bin/bash
red=$( tput setaf 1 )
green=$( tput setaf 2 )
NC=$( tput setaf 0 ) 

if [ "$(whoami)" != "root" ]; then
	  printf "%s\n" "${red}[ERROR] Sorry you are not root :("
	  printf "%s\n" "[ERROR] You have to be root so I can initialize your /etc/hosts${green}"
	exit 1
fi

printf "%s\n" "${green}Begin initialization..."

echo "[INFO] add host to /etc/hosts"
echo "127.0.0.1 api.instabugessamtest.com" >> /etc/hosts
echo "127.0.0.1 instabugessamtest.com" >> /etc/hosts
echo "[INFO] finished adding host"
echo "[SUCCESS] Initialization finished!"