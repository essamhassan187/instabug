class CreateStates < ActiveRecord::Migration[5.0]
  def up
    create_table :states do |t|
      
      t.string :device
      t.string :os
      t.integer :memory
      t.integer :storage
      t.belongs_to :bug, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
  def down
  	drop_table :states
  end
end
