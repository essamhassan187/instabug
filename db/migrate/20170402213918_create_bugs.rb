class CreateBugs < ActiveRecord::Migration
  def up
    create_table :bugs do |t|
      
      t.string :application_token
      t.integer :number
      t.column :status, :integer, default: 0, null: false
      t.column :priority, :integer, default: 0, null: false
      t.string :comment
      t.timestamps null: false
    end
  end
  def down
  	drop_table :bugs
  end
end