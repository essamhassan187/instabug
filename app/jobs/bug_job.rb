class BugWorker
  include Sneakers::Worker
  from_queue 'bugs'
  def work(msg)
    bug_params=JSON.parse msg
    Sneakers::logger.info "#{bug_params}"
    bug = Bug.new
    bug.assign_attributes bug_params
    Sneakers::logger.info " Inserting new bug #{bug.to_json}"
    bug.save
    Sneakers::logger.info " Bug #{bug.to_json} inserted successfully"
    ack!
  end
end