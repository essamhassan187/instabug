module Api::V1
  class BugsController < ApplicationController

    # POST /bugs
    def create
      @bug = Bug.new
          @bug.assign_attributes(bug_params)
          if @bug.valid?
            Sneakers.publish(bug_params.to_json,:routing_key=>'bugs')
            # will cause issue, need to investigate more
            render :json => { :number => @bug.number} 
          else
            render :json => { :error => 'Bad request'}, status: :bad_request
          end
    end

    # GET /bugs/:number?application_token
    def show
      @bug = Bug.find_by_application_token_and_number(
          params[:application_token],params[:id])
          if @bug
            @state = @bug.state
            render :json => { :bug => @bug.as_json( :include => {
              :state => {:except => [:created_at,:updated_at,:bug_id] } } )  }
          else
            render :json => { :error => 'not found'}, status: :not_found
          end
    end


    private
        def state_params
          params[:bug].require(:state).permit
        end
        def bug_params
          params[:bug].merge(application_token: params[:application_token] ).permit(:application_token,:priority,:comment,:status,:number,:state_attributes => [ :os,:device,:storage,:memory])
        end
  end
end
