class Bug < ApplicationRecord
  include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    settings index: {
      analysis: {
        analyzer: {
          ngram_analyzer: {
            tokenizer: 'ngram_tokenizer'
          }
        },
          tokenizer: {
            ngram_tokenizer: {
                type: "nGram",
                min_gram: "2",
                max_gram: "50"
            }
        }
      }
    } do
      mappings dynamic: 'false' do
        indexes :number
        indexes :application_token
        indexes :priority
        indexes :status
        indexes :comment, analyzer: 'ngram_analyzer'
      end
    end

  has_one :state
  accepts_nested_attributes_for :state

  enum status: [ :new_bug, :in_progress, :closed ]
  enum priority: [ :minor, :major, :critical ]

  validates_uniqueness_of :number, scope: :application_token
  validates :status, :priority, :application_token, presence: true
  before_validation :set_number
  def set_number
    $lock = RemoteLock.new(RemoteLock::Adapters::Redis.new($redis))
    $lock.synchronize("bug_number") do
      max_r = Bug.where(application_token: self.application_token).order(number: :desc).first
      unless max_r.nil?
        self.number = max_r.number + 1
      else
        self.number = 1
      end
    end
  end

end
Bug.import force: true