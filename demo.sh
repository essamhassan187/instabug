# !/bin/bash
red=$( tput setaf 1 )
green=$( tput setaf 2 )
NC=$( tput setaf 0 )

printf "%s\n" "${green}[INFO] Begin demo. [Enter] will advance you in demo"
read _
printf "%s\n" "[INFO] Reporting our first bug"
printf "%s\n" "${NC}{'application_token':'client1', 'bug':{ 'status':'in_progress','priority':'major','comment':'screen issue'}}${green}"
read _
curl -H "Content-Type: application/json" -X POST -d '{"application_token":"client1", "bug":{ "status":"in_progress","priority":"major","comment":"screen issue"}}' http://api.instabugessamtest.com:3000/v1/bugs/

read _
printf "\n%s\n" "[INFO] Looking up bugs"
printf "%s\n" "[INFO] endpoint GET /v1/bugs/"
printf "%s\n" "${NC}{'application_token':'client1', 'bug':{ 'status':'in_progress','priority':'major','comment':'screen issue'}}${green}"
read _
curl -X GET  http://api.instabugessamtest.com:3000/v1/bugs/1?application_token=client1

read _
printf "%s\n" "[INFO] Comment search "
printf "%s\n" "[INFO] endpoint GET /bugs/_search?pretty=1&q=comment:en"
read _
curl -X GET  http://instabugessamtest.com:9200/bugs/_search?pretty=1&q=comment:en