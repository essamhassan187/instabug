# README

Things you may want to cover:

* Ruby version 2.2

* Configuration run docker-compose up -d

* Database creation db:create

* Database initialization db:migrate

* How to run the test suite 

* Services (job queues, cache servers, search engines, etc.) RabbitMQ, Redis, Mysql, Elasticsearch

Test Instructions:
- Make sure ports 3000,3306,9200,4369,6379 are available on your machine 
 for docker-proxy bridge networking. Using direct port mapping for simplicity

* git clone [repo]
* cd repo
* sudo ./initialize.sh
* docker-compose up -d
* ./demo.sh
