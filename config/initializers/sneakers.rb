require 'sneakers'
Sneakers.configure( :heartbeat => 2,
                    :amqp => 'amqp://guest:guest@rabbitmq:5672',
                    :vhost => '/',
                    :exchange => 'sneakers',
                    :exchange_type => :direct )
Sneakers.logger.level = Logger::INFO